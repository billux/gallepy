#!/usr/bin/env python

from jinja2 import Environment, FileSystemLoader, StrictUndefined
import os
from PhotoView import PhotoView
import json
import codecs

TEMPLATE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')

class GalleryView:

    html_template = 'gallery.html.j2'
    html_story_template = 'story.html.j2'

    def __init__(self, settings, gallery):
        self.settings = settings
        self.gallery = gallery
        self.html_output_filename = os.path.join(self.gallery.rel_path, 'index.html')
        self.geojson_filename = os.path.join(self.gallery.rel_path, 'gallery.geojson')
        self.story_filename = os.path.join(self.gallery.rel_path, 'story.html')

    def build_html_page(self):

        """
        We check here if the index.html must be (re)built against multiple
        conditions, then call __build_html_page which effectively rebuild the
        page.
        """

        if self.gallery.has_changed:
            self.__build_html_page()

        elif not os.path.isfile(self.html_output_filename):
            self.__build_html_page()

        else:
            stat_html = os.stat(self.html_output_filename)
            stat_gallery = os.stat(self.gallery.rel_path)
            if stat_gallery.st_mtime > stat_html.st_mtime:
                self.__build_html_page()
        
    def __build_html_page(self):

        print('Building gallery ' + self.gallery.rel_path)
        env = Environment(loader=FileSystemLoader(TEMPLATE_DIR), undefined=StrictUndefined)
        template = env.get_template(self.html_template)
        template_vars = {
                'dirname': self.gallery.dirname,
                'rel_path': self.gallery.rel_path,
                'parents': self.gallery.parents,
                'title': self.gallery.attributes['title'],
                'description': self.gallery.attributes['description'],
                'date': self.gallery.attributes['date'],
                'location': self.gallery.attributes['location'],
                'root_uri': self.settings['global']['root_uri'],
                'thumbnail': self.gallery.attributes['thumbnail'],
                'galleries': self.gallery.galleries,
                'photos': self.gallery.photos,
                'has_story': self.gallery.has_story
                }
        html_output = template.render(template_vars)

        with codecs.open(self.html_output_filename, 'w+', 'utf8') as html_output_file:
            html_output_file.write(html_output)

    def build_sub(self):

        for photo in self.gallery.photos:
            photoView = PhotoView(self.settings, photo)
            photoView.build_html_page()

        for gallery in self.gallery.galleries:
            galleryView = GalleryView(self.settings, gallery)
            galleryView.build_html_page()
            galleryView.build_geojson_file()
            if gallery.has_story:
                galleryView.build_story_page()
            galleryView.build_sub()


    def build_geojson_file(self):

        """
        We check here if the geojson file must be (re)built against multiple
        conditions, then call __build_geojson_file which effectively rebuild the
        file.
        TODO: write a generic private method __check_rebuild() or so to decide
        when the destination should be rebuilt.
        """

        #if self.gallery.has_changed:
        #    self.__build_geojson_file()

        #elif not os.path.isfile(self.geojson_filename):
        if not os.path.isfile(self.geojson_filename):
            self.__build_geojson_file()

        #else:
        #    stat_geojson = os.stat(self.geojson_filename)
        #    stat_gallery = os.stat(self.gallery.rel_path)
        #    if stat_gallery.st_mtime > stat_html.st_mtime:
        #        self.__build_html_page()
        
    def build_geojson_file(self):
        with open(self.geojson_filename, 'w+') as geojson_file:
            geojson_file.write('var photos_geojson = { "type": "FeatureCollection", "features": ')
            json.dump(self.gallery.build_geojson(), geojson_file)
            geojson_file.write(" };\n")

    def build_story_page(self):
        """
        Build story page if HTML file doesn't exist or markdown file is newer
        than HTML file.
        """
        if not os.path.isfile(self.story_filename):
            self.__build_story_page()
        else:
            stat_html = os.stat(self.story_filename)
            stat_mdwn = os.stat(os.path.join(self.gallery.rel_path, 'story.mdwn'))
            if stat_mdwn.st_mtime > stat_html.st_mtime:
                self.__build_story_page()

    def __build_story_page(self):
        with open(self.story_filename, 'w+') as story_file:
            env = Environment(loader=FileSystemLoader(TEMPLATE_DIR), undefined=StrictUndefined)
            template = env.get_template(self.html_story_template)
            template_vars = {
                'dirname': self.gallery.dirname,
                'rel_path': self.gallery.rel_path,
                'parents': self.gallery.parents,
                'title': self.gallery.attributes['title'],
                'description': self.gallery.attributes['description'],
                'date': self.gallery.attributes['date'],
                'location': self.gallery.attributes['location'],
                'root_uri': self.settings['global']['root_uri'],
                'html_story': self.gallery.build_story()
                }
        html_output = template.render(template_vars)

        with open(self.story_filename, 'w+') as story_file:
            story_file.write(html_output)
