/*
 * Create the map.
 */
var map = L.map('map', {zoomControl: false});
L.control.scale({"imperial":false}).addTo(map);

/*
 * Define multiple map underlays and add them to the map.
 */
var OSMLayer = L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
        });
var IGNLayer = L.tileLayer('http://gpp3-wxs.ign.fr/l8l4bs85uejc1eqwaw5l5z5x/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}', {
        attribution: '&copy; <a href="http://www.ign.fr/">IGN France</a>, &copy; <a href="http://www.geoportail.fr/">Geoportail</a>'
        });
var THOutdoorsLayer = L.tileLayer('https://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=2a342dee09ea491d9bfbedb55c50ce8d', {
        attribution: '&copy; <a href="https://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="https://openstreetmap.org">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>'
        });
var NRCanLayer = L.tileLayer('http://wms.ess-ws.nrcan.gc.ca/wms/toporama_en?service=wms&request=GetMap&version=1.1.1&format=image/png&srs=epsg:4326&layers=WMS-Toporama&width=256&height=256&bbox=', {
        attribution: ''
        });
THOutdoorsLayer.addTo(map);

var baseLayers = {
    "OSM Outdoors Map": THOutdoorsLayer,
    "Topo IGN France": IGNLayer,
    "OSM standard": OSMLayer,
    "National Resources Canada": NRCanLayer
};

var overlays = {};

/*
 * Define settings for markers.
 */
var markerOptions = {
    radius: 6,
    fillColor: "#7D7D7D",
    color: "#555555",
    weight: 1,
    opacity: 1,
    fillOpacity: 1,
};

var GPSMarkerOptions = {
    startIconUrl: null,
    endIconUrl: null,
    shadowUrl: null
};

var GPSPolylineOptions = {
    color: '#337AB7',
    clickable: false,
}

/*
 * Import GPS track if the file exists and add it to the map.
 */
var GPSTrack = new L.GPX('track.gpx', {
    async: false,
    marker_options: GPSMarkerOptions,
    polyline_options: GPSPolylineOptions
});
if (GPSTrack.get_distance() > 0) {
    GPSTrack.addTo(map);
    overlays["Trace GPS"] = GPSTrack;
}

/*
 * Create the FeatureGroup object from loaded GeoJSON.
 */
var photos = L.geoJson(photos_geojson, {
            /*
             * If the feature is actually the photo currently viewed (we refer to
             * the URL), then style it with a different color.
             */
            style: function (feature) {
                if (window.document.title.indexOf(feature.properties.name) >= 0) {
                    return {zIndexOffset: 1000, fillColor: "#EBF633", radius: 8};
                }
            },

            /*
             * Create a marker for each feature.
             */
            pointToLayer: function (feature, latlng) {
                if (feature.properties.type === 'photo') {
                    return L.circleMarker(latlng, markerOptions);
                }
                else if (feature.properties.type === 'gallery') {
                    return L.marker(latlng);
                }
            },

            /* And make the marker clickable. It displays the photo inside a
             * popup, plus a link to photo's page.
             */
            onEachFeature: function (feature, layer) {
                layer.bindPopup(feature.properties.popupContent);
            }
        }

/*
 * Finally, add it to the map.
 */
).addTo(map);
overlays["Photos"] = photos;

/*
 * If GPS trace has been loaded succesfully, set the map bounds to contain
 * entire trace, else set the map bounds to contain all photos.
 * fitBounds() set the maximum zoom possible, but this is not very usefull, so
 * then zoom out if zoom is too high.
 */
if (GPSTrack.get_distance() > 0) {
        map.fitBounds(GPSTrack.getBounds());
}
else if (photos.getLayers().length == 1) {
        map.fitBounds(photos.getBounds());
        map.setZoom(11);
}
else {
        map.fitBounds(photos.getBounds());
}
if (map.getZoom() > 14) {
        map.setZoom(14);
}

/*
 * If we zoom out to far, do not show photo's markers.
 */
//map.on('zoomend', function() {
//    if (map.getZoom() >= 9) {
//        map.addLayer(photos);
//    }
//    else {
//        map.removeLayer(photos);
//    }
//});

/*
 * Add the overlay control to the map.
 */
L.control.layers(baseLayers, overlays).addTo(map);
