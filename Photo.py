#!/usr/bin/env python

import exifread
import os

class Photo:

    def __init__(self, rel_path, parents=None):

        self.rel_path  = rel_path
        self.filename  = os.path.basename(rel_path)
        self.thumbnail = os.path.join(os.path.dirname(self.rel_path), '.thumb', self.filename)
        self.websize   = os.path.join(os.path.dirname(self.rel_path), '.websize', self.filename)

        self.parents = parents

        self.exif_tags = self.__read_exif_data()

    def __read_exif_data(self):
        f = open(self.rel_path, 'rb')
        exif_data = exifread.process_file(f, details=False)

        if 'Image ImageDescription' not in exif_data or str(exif_data['Image ImageDescription']) == 'None':
            exif_data['Image ImageDescription'] = ''

        return exif_data
