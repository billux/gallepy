#
# This is a recursive Makefile to:
#   - retrieve photos from your camera
#   - optimize JPEG, add/remove EXIF tags
#   - build thumbnail and web friendly sizes from original photos
#   - finally call gallepy to build HTML pages.
#


#
# Define here your camera's filesystem UUID (lsblk -o +UUID as root lists
# them) and the absolute path (when the filesystem is mounted) where photos are
# stored.
# You also need to add a line like this to your fstab to allow mounting by
# regular users:
#

CAM_UUID  = F84E-1690
CAM_STORE = /mnt/cam0/DCIM/100MSDCF/
#CAM_STORE = /home/romain/media/photos/public/quebec/99-misc/


# Clear out default implicit rules and variables.
MAKEFLAGS += --no-builtin-rules --no-builtin-variables


photos    = $(wildcard *.jpg)
galleries = $(filter %/, $(wildcard */)) # TODO: exclude special directories

.PHONY: all thumbs websize galleries gallepy retrieve creategallery deploy deploy-full

ifeq ($(MAKELEVEL),0)
export INITDIR = $(CURDIR)
all: thumbs websize galleries gallepy
else
all: thumbs websize galleries
endif


#
# Download photos from camera.
#
# This target is only called explicitly. You must pass $from and $gallery
# variables.
#

# If from date isn't specified, retrieve all files.
ifndef from
from = "1970-01-01"
endif

photos_src = $(shell find $(CAM_STORE) -maxdepth 1 -type f -newerct $(from))
photos_dst = $(patsubst %,$(gallery)/%.jpg, $(notdir $(basename $(photos_src))))

#mount_cam:
#	#findmnt -S UUID=$(CAM_UUID) >/dev/null || mount -U $(CAM_UUID)


#
# Create the specified gallery.
#
# Called automatically by retrieve if the gallery doesn't exist. The recipe could also be
# called manually like this:
#
#     $ make creategallery gallery=your-gallery
#

creategallery: $(gallery)
$(gallery):
	mkdir -p $(gallery)
	mkdir $(gallery)/.thumb $(gallery)/.websize
	cp gallery.ini.tpl $(gallery)/gallery.ini
	echo "atom_pubdate = $(date +'%F %T %z')" >> $(gallery)/gallery.ini
	$(EDITOR) $(gallery)/gallery.ini
	@echo "Copy your GPX track into track.gpx if you have one!"


#
# Retrieve photos, usually from  mounted camera.
#
retrieve: $(gallery) $(photos_dst)
$(photos_dst): $(gallery)/%.jpg: $(CAM_STORE)/%.JPG
	@#jpegtran -optimize -progressive -copy all -outfile $@ $<
	@convert -interlace Plane -quality 80 $< $@
	@exiv2 -M" add Exif.Image.Artist Photographer, Romain Dessort" $@
	@exiv2 -M" add Exif.Image.Copyright Copyright, Romain Dessort, $(date +%Y). This photo is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License. See https://creativecommons.org/licenses/by-sa/3.0/ for details." $@
	@#iptc_title=$$(exiv2 -K Iptc.Application2.Caption -Pv $@ 2>/dev/null)
	@#[ -n "$$iptc_title" ] && exiv2 -M" set Exif.Image.ImageDescription $$iptc_title" $@
	@#[ -f "$(gallery)/track.gpx" ] && gpscorrelate -g $(gallery)/track.gpx -z -1 $(gallery)/*.jpg
	@mogrify -auto-orient $@
ifdef delete_src
	rm $<
endif
	@#unmount -U $(CAM_UUID)

#
# Deploy gallery files to remote webserver
#
deploy:
	rsync -Lav --delete --exclude gallepy.py --exclude "*.ini" --exclude Makefile --exclude gallery.ini.tpl --exclude bad --include "**/.thumb/*.jpg" --include ".websize/*.jpg" --exclude "*.jpg" ./ idunn.univers-libre.net:www/photos/

deploy-full:
	rsync -Lav --delete --exclude gallepy.py --exclude "*.ini" --exclude Makefile --exclude gallery.ini.tpl --exclude bad ./ idunn.univers-libre.net:www/photos/
	#
#
# Generate thumbnails and websize images from original photos.
#

# Set thumbnail path
thumbs  = $(addprefix .thumb/,  $(photos))
websize = $(addprefix .websize/, $(photos))

thumbs: $(thumbs)
$(thumbs): .thumb/%.jpg: %.jpg
	convert -thumbnail x148 $< $@

websize: $(websize)
$(websize): .websize/%.jpg: %.jpg
	convert -interlace Plane -quality 80 -resize x900 $< $@

# Recursive call of make from sub-galleries
galleries:
	@for gallery in $(galleries); do \
		if [ "$$gallery" != "bad/" ]; then \
			$(MAKE) -C $$gallery -f $(INITDIR)/Makefile; \
		fi; \
	done


#
# Build all HTML pages with gallepy.
#
# This recipe is only called on level 0 of make recursion, since gallepy has to
# be called only one time, at the top of the gallery hierarchy.
#

gallepy:
	./gallepy.py
