Gallepy is a simple static gallery generator written in Python. It was designed to use the filesystem hierarchy to create galleries and sub-galleries (without depth limit).

All geotagged photos are shown on a map (using Leaflet, multiple layers available) and a GPX track can be added to the map. Also, each gallery may have a story (from a markdown formatted file) in them, displayed in a second tab in the gallery view.

Gallepy was developed for my own use to share my photo galleries. You can find it at https://photos.univers-libre.net/.