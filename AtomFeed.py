#!/usr/bin/env python

from feedgen.feed import FeedGenerator

class AtomFeed:

    def __init__(self, settings):

        self.settings = settings
        self.fg = FeedGenerator()

        self.fg.id(self.settings['atom_feed']['url'])
        self.fg.title(self.settings['atom_feed']['title'])
        self.fg.author(name=self.settings['atom_feed']['author'])
        self.fg.link(href=self.settings['atom_feed']['url'], rel='alternate')
        self.fg.link(href=self.settings['atom_feed']['url'] + '/atom.xml', rel='self')
        self.fg.language(self.settings['atom_feed']['language'])

    def add_entry(self, entry):
        fe = self.fg.add_entry()
        fe.title(entry.attributes['title'])
        fe.summary(summary=entry.attributes['description'])
        fe.content(src=self.settings['atom_feed']['url'] + entry.rel_path)
        fe.link(link={'href': self.settings['atom_feed']['url'] + entry.rel_path})
        fe.guid(guid=entry.rel_path)
        fe.published(published=entry.attributes['atom_pubdate'])
        fe.updated(updated=entry.attributes['atom_pubdate'])
        #fe.updated(entry.mtime)

    def build_atom_feed(self):
        self.fg.atom_file('atom.xml', pretty=True)
