#!/usr/bin/env python

from Photo import Photo
import os
import configparser
from utils import exif_gps_to_degree
import markdown
import codecs

class Gallery:

    def __init__(self, settings, rel_path, parents=[]):
        self.settings = settings
        self.rel_path = rel_path
        self.dirname  = os.path.basename(self.rel_path)

        self.parents = parents

        self.attributes = self.__load_ini_file()
        self.photos = self.__get_photos()
        self.galleries = self.__get_galleries()

        if 'thumbnail' not in self.attributes or self.attributes['thumbnail'] == '':
            if len(self.photos) > 0:
                self.attributes['thumbnail'] = self.photos[0].thumbnail
            else:
                self.attributes['thumbnail'] = 'static/gallery-default.png'

        if os.path.isfile(os.path.join(self.rel_path, 'story.mdwn')):
            self.has_story = True
        else:
            self.has_story = False

        self.has_changed = False

    def __load_ini_file(self):
        attributes = configparser.ConfigParser()
        with codecs.open(os.path.join(self.rel_path, 'gallery.ini'), 'r', 'utf8') as attributes_file:
            attributes.read_file(attributes_file)

        if 'location' in attributes['gallery'] \
        and (not 'lng' in attributes['gallery'] or attributes['gallery']['lat'] == '') \
        and (not 'lat' in attributes['gallery'] or attributes['gallery']['lng'] == ''):
            from pynominatim import Nominatim
            nominatim = Nominatim()
            nom_result = nominatim.query(attributes['gallery']['location'])
            if nom_result and len(nom_result) != 0:
                attributes['gallery']['lat'] = nom_result[0]['lat']
                attributes['gallery']['lng'] = nom_result[0]['lon']
            else:
                print("Failed to geocode " + attributes['gallery']['location'])
        return attributes['gallery']

    def __get_galleries(self):
        galleries = []
        for entry in sorted(os.listdir(self.rel_path)):

            entry_path = os.path.normpath(os.path.join(self.rel_path, entry))
            if os.path.isdir(entry_path) and os.path.basename(entry_path) not in self.settings['global']['exclude_dir']:

                # Clone self.parents list to entry_parents so that we can
                # insert at the beginning this gallery object, which is the
                # parent of Gallery we are going to instantiate.
                entry_parents = list(self.parents)
                entry_parents.insert(0, self)
                gallery = Gallery(self.settings, entry_path, entry_parents)
                galleries.append(gallery)

        return galleries

    def __get_photos(self):
        photos = []
        for entry in sorted(os.listdir(self.rel_path)):

            entry_path = os.path.join(self.rel_path, entry)
            if os.path.isfile(entry_path) and (entry.split('.')[-1] == 'jpg' or entry.split('.')[-1] == 'JPG'):

                # Clone self.parents list to entry_parents so that we can
                # insert at the beginning this gallery object, which is the
                # parent of Photo we are going to instantiate.
                entry_parents = list(self.parents)
                entry_parents.insert(0, self)
                photo = Photo(entry_path, entry_parents)
                photos.append(photo)
        return photos

    def next(self, object):
        if object.__class__.__name__ == 'Photo':
            stop = False
            for photo in self.photos:
                if stop == True:
                    return photo
                if photo == object:
                    stop = True
        elif object.__class__.__name__ == 'Gallery':
            pass
        else:
            raise Exception('Could not get the next element for object type %s', object.__class__.__name__)

    def prev(self, object):
        if object.__class__.__name__ == 'Photo':
            previous = None
            for photo in self.photos:
                if photo == object:
                    return previous
                else:
                    previous = photo
        elif object.__class__.__name__ == 'Gallery':
            pass
        else:
            raise Exception('Could not get the previous element for object type %s', object.__class__.__name__)


    def build_geojson(self):

        features = []

        # For the root gallery, show all galleries of its galleries on the map.
        if len(self.parents) == 0:
            for gallery in self.galleries:
                for feature in gallery.build_geojson():
                    features.append(feature)

        else:
            for gallery in self.galleries:
                if 'lat' in gallery.attributes and gallery.attributes['lat'] != 0:
                    feature = {
                        'type': 'Feature',
                        'properties': {
                            'name': gallery.attributes['title'],
                            'popupContent': '<a href="' + self.settings['global']['root_uri'] + gallery.rel_path + '">' + gallery.attributes['title'] + '</a>',
                            'type': 'gallery'
                            },
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [gallery.attributes['lng'], gallery.attributes['lat']]
                            }
                        }

                    features.append(feature)

            for photo in self.photos:
                if 'GPS GPSLatitude' in photo.exif_tags and 'GPS GPSLongitude' in photo.exif_tags:
                    lat = exif_gps_to_degree(photo.exif_tags['GPS GPSLatitude'], photo.exif_tags['GPS GPSLatitudeRef'])
                    lng = exif_gps_to_degree(photo.exif_tags['GPS GPSLongitude'], photo.exif_tags['GPS GPSLongitudeRef'])

                    feature = {
                        'type': 'Feature',
                        'properties': {
                            'name': photo.filename,
                            'type': 'photo',
                            'popupContent': '<a href="' + photo.filename.replace('.jpg', '.html') + '"><img src="' + self.settings['global']['root_uri'] + photo.thumbnail + '" alt="' + photo.filename + '" width="100" /></a>'
                            },
                        'geometry': {
                            'type': 'Point',
                            'coordinates': [lng, lat]
                            }
                        }

                    features.append(feature)

            # If photos are not geotagged, add a marker for the current gallery.
            if len(features) == 0 \
                    and 'lat' in self.attributes and self.attributes['lat'] != 0:
                feature = {
                    'type': 'Feature',
                    'properties': {
                        'name': self.attributes['title'],
                        'popupContent': self.attributes['title'],
                        'type': 'gallery'
                        },
                    'geometry': {
                        'type': 'Point',
                        'coordinates': [self.attributes['lng'], self.attributes['lat']]
                        }
                    }

                features.append(feature)


        return features

    def build_story(self):
        with open(os.path.join(self.rel_path, 'story.mdwn')) as story_markdown_file:
            story_markdown = story_markdown_file.read()
            story_html = markdown.markdown(story_markdown)

        return story_html

