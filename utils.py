#!/usr/bin/env python

import os


class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)


def exif_gps_to_degree(coord, ref):

    # Parse coordinate
    coord = str(coord).lstrip('[').rstrip(']')
    deg, min, sec = coord.split(', ')
    s_t, s_b = sec.split('/')
    sec = float(s_t) / float(s_b)
    dec_degree = float(deg) + float(min)/60 + float(sec)/3600
    if str(ref) == 'W' or str(ref) == 'S':
        dec_degree = -dec_degree 
    return dec_degree
