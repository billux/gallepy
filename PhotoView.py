#!/usr/bin/env python

from jinja2 import Environment, FileSystemLoader, StrictUndefined
import os
import time
import locale

TEMPLATE_DIR = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
class PhotoView:

    html_template = 'photo.html.j2'

    def __init__(self, settings, photo):
        self.settings = settings
        self.photo = photo
        self.rel_path = os.path.join(os.path.dirname(self.photo.rel_path), self.photo.filename.split('.')[0] + '.html')

    def build_html_page(self):

        """
        We check here if the <photo>.html must be (re)built against multiple
        conditions, then call __build_html_page which effectively rebuild the
        page.
        """

        if not os.path.isfile(self.rel_path):
            self.__build_html_page()

        else:
            stat_html = os.stat(self.rel_path)
            stat_photo = os.stat(self.photo.rel_path)
            if stat_photo.st_mtime > stat_html.st_mtime:
                self.__build_html_page()

    def __build_html_page(self):

        #print(self.photo.rel_path)
        #for key in self.photo.exif_tags:
        #    print(key, ': ', self.photo.exif_tags[key])

        if 'EXIF DateTimeDigitized' in self.photo.exif_tags:
            locale.setlocale(locale.LC_TIME, "fr_CA.UTF-8")
            photo_time = time.strptime(str(self.photo.exif_tags['EXIF DateTimeDigitized']), "%Y:%m:%d %H:%M:%S")
            pretty_date = time.strftime("%A %d<br />%B<br />%Y", photo_time)
            pretty_time = time.strftime("%H:%M", photo_time)
        else:
            pretty_date = '<br />'
            pretty_time = '<br />'

        env = Environment(loader=FileSystemLoader(TEMPLATE_DIR), undefined=StrictUndefined)
        template = env.get_template(self.html_template)
        template_vars = {
                'filename': self.photo.filename,
                'rel_path': self.photo.rel_path,
                'websize': self.photo.websize,
                'parents': self.photo.parents,
                'exif_tags': self.photo.exif_tags,
                'root_uri': self.settings['global']['root_uri'],
                'next': self.photo.parents[0].next(self.photo),
                'previous': self.photo.parents[0].prev(self.photo),
                'date': pretty_date,
                'time': pretty_time
                }
        html_output = template.render(template_vars)

        with open(self.rel_path, 'w+') as html_output_file:
            html_output_file.write(html_output)

        self.photo.parents[0].has_changed = True
