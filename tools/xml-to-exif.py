#!/usr/bin/env python2

import xml.etree.ElementTree as ET
import pyexiv2
import sys
import os

def main():
    dir = sys.argv[1]
    tree = ET.parse(os.path.join(dir, 'index.xml'))
    root = tree.getroot()
    for item in root.iter('item'):

        # Get title and description from XML file
        photo_filename = item.get('src')
        photo_title = item.find('title').text
        photo_desc = item.find('title_description').text

        # Get title from Iptc.Application2.Caption tag
        metadata = pyexiv2.ImageMetadata(os.path.join(dir, photo_filename))
        metadata.read()
        iptc_title = None
        if 'Iptc.Application2.Caption' in metadata:
            iptc_title = metadata['Iptc.Application2.Caption']

        if photo_title and photo_title != ' ':

            if photo_desc and photo_desc != ' ':
                photo_title = photo_title + '. ' + photo_desc

            if 'Exif.Image.ImageDescription' in metadata:
                exif_title = metadata['Exif.Image.ImageDescription']
                exif_title.value = photo_title
            else:
                metadata['Exif.Image.ImageDescription'] = pyexiv2.ExifTag('Exif.Image.ImageDescription', photo_title)

            metadata.write()

        elif iptc_title and iptc_title.value and iptc_title.value != None and iptc_title.value != '':
            #exif_title = metadata['Exif.Image.ImageDescription']
            #exif_title.value = iptc_title.value
            metadata['Exif.Image.ImageDescription'] = pyexiv2.ExifTag('Exif.Image.ImageDescription', iptc_title.value)
            metadata.write()


if __name__ == '__main__':
    main()
