#!/bin/sh

find -name gallery.ini |while read ini; do
    galdir=$(dirname $ini)
    lastphoto=$(ls $galdir/*.jpg 2>/dev/null |tail -1)
    [ -z "$lastphoto" ] && continue
    date=$(exiv2 $lastphoto |awk  '/^Image timestamp/{print $4,$5}')
    sed -i "s/^atom_pubdate.*/atom_pubdate = $date -0400/" $ini
done
