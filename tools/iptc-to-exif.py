#!/usr/bin/env python2

import os
import sys
import pyexiv2

def main():
    dir = sys.argv[1]
    for photo_filename in os.listdir(dir):

        if os.path.isfile(os.path.join(dir, photo_filename)) and (photo_filename.split('.')[-1] == 'jpg' or photo_filename.split('.')[-1] == 'JPG'):
            print(os.path.join(dir, photo_filename))
            # Get title from Iptc.Application2.Caption tag
            metadata = pyexiv2.ImageMetadata(os.path.join(dir, photo_filename))
            metadata.read()
            iptc_title = None
            if 'Iptc.Application2.Caption' in metadata:
                iptc_title = metadata['Iptc.Application2.Caption']

            if iptc_title and iptc_title.value and iptc_title.value != None and iptc_title.value != '':
                #exif_title = metadata['Exif.Image.ImageDescription']
                #exif_title.value = iptc_title.value
                metadata['Exif.Image.ImageDescription'] = pyexiv2.ExifTag('Exif.Image.ImageDescription', iptc_title.value)
                metadata.write()


if __name__ == '__main__':
    main()

