#!/usr/bin/env python3

from Photo import Photo
from Gallery import Gallery
from PhotoView import PhotoView
from GalleryView import GalleryView
from AtomFeed import AtomFeed
from utils import cd
import configparser
import os.path

CONFIG_FILENAME = 'settings.ini'

def get_children_galleries(gallery):

    children_galleries = []

    for gal in gallery.galleries:

        # If the gallery has no children galleries, add it to the Atom
        # feed. We add only children galleries.
        if len(gal.galleries) == 0:
            children_galleries.append(gal)
        else:
            children_galleries.extend(get_children_galleries(gal))

    return children_galleries


def main():

    # Make it possible to pass as argument, else assume it is pwd.
    gallery_root = '/home/romain/tmp/99-misc/'
    gallery_root = '.'

    with cd(gallery_root):

        # Load settings from config file.
        settings = configparser.ConfigParser()
        with open(CONFIG_FILENAME, 'r') as config_file:
            settings.read_file(config_file)

        gallery = Gallery(settings, '.')
        galleryView = GalleryView(settings, gallery)

        galleryView.build_html_page()
        galleryView.build_geojson_file()
        galleryView.build_sub()

        print('Building atom feed')
        atomFeed = AtomFeed(settings)
        for gal in get_children_galleries(gallery):
            atomFeed.add_entry(gal)
        atomFeed.build_atom_feed()

if __name__ == '__main__':
    main()
